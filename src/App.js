import React, {useState} from 'react';
import './App.css';
import FibonacciButton from './components/FibonacciButton';

function App() {

  const [ nthFibonacciNumber, setNthFibonacciNumber ] = useState(0);

  return (
    <div className={ `App`}>
      <div>
      { nthFibonacciNumber }
      </div>
      <FibonacciButton setNthFibonacciNumber={setNthFibonacciNumber}/>
    </div>
  );
}

export default App;
