import { useState } from 'react';

const FibonacciButton = ({ setNthFibonacciNumber} ) => {

    const [ fibonacciCount, setFibonacciCount ] = useState(0);

    const getFibonacciNumber = (n) => {
            // returns the n-th number in the Fibonacci Sequence
            let x = 0, y = 1;

            for (let i = 0; i < n; i++) {
                let temp = x;
                x = y;
                y = y + temp;
            }

            return x;
    };

    const setNextFibonacciNumber = () => {
        const newFibonacciCount = fibonacciCount+1,
            nextFibonacciNumber = getFibonacciNumber(newFibonacciCount);

        setFibonacciCount(newFibonacciCount);
        setNthFibonacciNumber(nextFibonacciNumber);
    };

    return <button onClick={setNextFibonacciNumber}>Show me the next fibonacci number</button>

};

export default FibonacciButton;
